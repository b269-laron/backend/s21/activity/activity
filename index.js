let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

function add_users(fighter){
    users[users.length] = fighter;

}
add_users('John Cena');
console.log(users)

function get_user(index){
return users[index];
}

console.log(get_user(2));

function delete_user(){
    let deleted_user = users[users.length -1];
    users.length--
    return deleted_user;
} 
console.log(delete_user());
console.log(users);

function update_user(updated_fighter, index){
    users[index] = updated_fighter;
}
update_user('MJF', 3);
console.log(users)

function delete_users(){
    users = [];
}
delete_users();
console.log(users);

function isUsersEmpty (){
    if (users.length > 0){
        return false
        } else {
            return true
        }
}
console.log(isUsersEmpty());


users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

function get_users_index(item){
    for (let index = 0; index < users.length; index++){
        if(item === users[index]){
            return index;
        }
    }
}

console.log(get_users_index('Dwayne Johnson'));